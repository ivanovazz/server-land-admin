package com.serverlandadmin.serverlandadmin.component;

import com.serverlandadmin.serverlandadmin.dto.Order;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;
import static com.serverlandadmin.serverlandadmin.configuration.ActiveMQConfig.ORDER_QUEUE;
import javax.jms.Session;

@Component
public class OrderConsumer {
    private static Logger log = LoggerFactory.getLogger(OrderConsumer.class);

    @JmsListener(destination = ORDER_QUEUE)
    public void receiveMessage(@Payload Order order,
                               @Headers MessageHeaders headers,
                               Message message, Session session) {
        log.info("content <" + order.getContent() + ">");


    }
}
