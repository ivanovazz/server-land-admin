package com.serverlandadmin.serverlandadmin.component;

import com.serverlandadmin.serverlandadmin.controller.exception.ResponseException;
import com.serverlandadmin.serverlandadmin.dto.BuildMessage;
import com.serverlandadmin.serverlandadmin.entity.BuildVersion;
import com.serverlandadmin.serverlandadmin.repository.BuildVersionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import javax.jms.Session;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.sql.Timestamp;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static com.serverlandadmin.serverlandadmin.configuration.ActiveMQConfig.ORDER_QUEUE;
@Component
public class BuildConsumer {
    private static Logger log = LoggerFactory.getLogger(BuildConsumer.class);
    private final BuildVersionRepository buildVersionRepository;

    public BuildConsumer (BuildVersionRepository buildVersionRepository) {
        this.buildVersionRepository = buildVersionRepository;
    }

    @JmsListener(destination = ORDER_QUEUE)
    public void receiveMessage(@Payload BuildMessage buildMessage,
                               @Headers MessageHeaders headers,
                               Message message, Session session) {
        log.info("content < version: " + buildMessage.getVersion() + " path: " + buildMessage.getDirPath() + ">");
        try {
            String sourceFile = buildMessage.getDirPath();
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());

            String fileName = buildMessage.getLandName() + "-" + buildMessage.getVersion() + "-" + timestamp.getTime() + ".zip";
            FileOutputStream fos = new FileOutputStream(buildMessage.getArtifactPath() + "/" + fileName);
            ZipOutputStream zipOut = new ZipOutputStream(fos);
            File fileToZip = new File(sourceFile);

            Boolean zipFlag = zipFile(fileToZip, fileToZip.getName(), zipOut);
            zipOut.close();
            fos.close();
            if (zipFlag) {
                BuildVersion buildVersion = buildVersionRepository.findById(buildMessage.getBuildId()).orElse(null);
                buildVersion.setActive(true);
                buildVersionRepository.saveAndFlush(buildVersion);
            } else {
                String errorMsg = "Zip cann't create!!!";
                BuildVersion buildVersion = buildVersionRepository.findById(buildMessage.getBuildId()).orElse(null);
                buildVersion.setContent(errorMsg);
                buildVersionRepository.saveAndFlush(buildVersion);
                throw new ResponseException(errorMsg);
            }
        } catch (Throwable e) {
            log.error(e.getMessage());
        }


    }

    private static Boolean zipFile(File fileToZip, String fileName, ZipOutputStream zipOut) {
        if (fileToZip.isHidden()) {
            return false;
        }
        try {
            if (fileToZip.isDirectory()) {
                if (fileName.endsWith("/")) {
                    zipOut.putNextEntry(new ZipEntry(fileName));
                    zipOut.closeEntry();
                } else {
                    zipOut.putNextEntry(new ZipEntry(fileName + "/"));
                    zipOut.closeEntry();
                }
                File[] children = fileToZip.listFiles();
                for (File childFile : children) {
                    zipFile(childFile, fileName + "/" + childFile.getName(), zipOut);
                }
                return true;
            }
            FileInputStream fis = new FileInputStream(fileToZip);
            ZipEntry zipEntry = new ZipEntry(fileName);
            zipOut.putNextEntry(zipEntry);
            byte[] bytes = new byte[1024];
            int length;
            while ((length = fis.read(bytes)) >= 0) {
                zipOut.write(bytes, 0, length);
            }
            fis.close();
            return true;
        } catch (Throwable e) {
            log.info(e.getMessage());
            return false;
        }

    }
}
