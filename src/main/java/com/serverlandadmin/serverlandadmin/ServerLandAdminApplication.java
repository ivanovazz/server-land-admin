package com.serverlandadmin.serverlandadmin;

import com.serverlandadmin.serverlandadmin.dto.Order;
import com.serverlandadmin.serverlandadmin.service.OrderSenderService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.concurrent.TimeUnit;

@SpringBootApplication
@RestController
public class ServerLandAdminApplication {
	private static Logger log = LoggerFactory.getLogger(ServerLandAdminApplication.class);

	@Autowired
	private OrderSenderService orderSenderServiceImpl;

	public static void main(String[] args) {
		SpringApplication.run(ServerLandAdminApplication.class, args);
	}

	@GetMapping("/hello")
	public String hello(@RequestParam(value = "name", defaultValue = "World") String name) {
		return String.format("Hello %s!", name);
	}
//	@GetMapping("/hello")
//	public String hello(@RequestParam(value = "name", defaultValue = "World") String name, String qwer) {
//		return String.format("Hello %s!", name);
//	}

	@GetMapping("/run")
	public String run() throws Exception {
		log.info("Spring Boot Embedded ActiveMQ Configuration Example");

		for (int i = 0; i < 5; i++) {
			Order myMessage = new Order(i + " - test", new Date());
			orderSenderServiceImpl.send(myMessage);
		}

		log.info("Waiting for all ActiveMQ JMS Messages to be consumed");
		TimeUnit.SECONDS.sleep(3);

		return "done";
	}
}
