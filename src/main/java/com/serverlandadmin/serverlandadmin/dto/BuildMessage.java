package com.serverlandadmin.serverlandadmin.dto;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
@Data
public class BuildMessage {
    private String dirPath;
    @Value( "${artifact.path}" )
    private String artifactPath;
    private String version;
    private String landName;
    private Integer buildId;


    public BuildMessage(String dirPath, String version, String landName, Integer buildId) {
        this.dirPath = dirPath;
        this.version = version;
        this.landName = landName;
        this.buildId = buildId;
    }
}
