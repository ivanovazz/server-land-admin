package com.serverlandadmin.serverlandadmin.controller;

import com.serverlandadmin.serverlandadmin.entity.User;
import com.serverlandadmin.serverlandadmin.repository.UserRepository;
import com.serverlandadmin.serverlandadmin.service.OrderSenderService;
import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Proxy;
import java.util.UUID;

@RestController
@Slf4j
@RequestMapping("/test")
public class TestController {

    private final UserRepository userRepository;
    private final OrderSenderService orderSenderService;

    @Autowired
    public TestController(UserRepository userRepository, OrderSenderService orderSenderService) {
        this.userRepository = userRepository;
        this.orderSenderService = orderSenderService;
    }

//    @GetMapping(value = "/test")
//    public String testMethod() {
//        OrderSenderService proxied = (OrderSenderService) Proxy.newProxyInstance(
//                orderSenderService.getClass().getClassLoader(),
//                orderSenderService.getClass().getInterfaces(),
//                (proxy, method, args) -> {
//                    log.info("Before call invoke in proxy (BEGIN)");
//                    Object result = method.invoke(orderSenderService, args);
//                    log.info("After call invoke in proxy (COMMIT)");
//
//                    return result;
//                }
//        );
//
//
//        proxied.showClass();
//        log.info(orderSenderService.getClass().getName());
//
//        return "OK";
//    }
    @PostMapping(value = "/test")
    public TestDTO testMethod(
        @RequestBody TestDTORequest request
    ) {
        User user = userRepository.findById(1).orElse(null);

        return TestDTO.builder()
            .userId(request.getUserId())
            .build();
    }

    @Data
    @Builder
    public static class TestDTO {
        private String testString;
        private Integer testInt;
        private UUID userId;
    }

    @Data
    public static class TestDTORequest {
        private UUID userId;
    }

}
