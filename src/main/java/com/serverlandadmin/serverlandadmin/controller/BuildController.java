package com.serverlandadmin.serverlandadmin.controller;

import com.serverlandadmin.serverlandadmin.controller.exception.ResponseException;
import com.serverlandadmin.serverlandadmin.dto.BuildMessage;
import com.serverlandadmin.serverlandadmin.entity.BuildVersion;
import com.serverlandadmin.serverlandadmin.entity.Land;
import com.serverlandadmin.serverlandadmin.repository.BuildVersionRepository;
import com.serverlandadmin.serverlandadmin.repository.LandRepository;
import com.serverlandadmin.serverlandadmin.service.BuildSenderService;
import com.serverlandadmin.serverlandadmin.service.BuildSenderServiceInterface;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/build")
public class BuildController {

    private final LandRepository landRepository;
    private final BuildVersionRepository buildVersionRepository;
    private final BuildSenderService buildSenderService;
    @Value( "${land.path}" )
    private String landPath;

    public BuildController(LandRepository landRepository,
                           BuildVersionRepository buildVersionRepository, BuildSenderService buildSenderService){
        this.landRepository = landRepository;
        this.buildVersionRepository = buildVersionRepository;
        this.buildSenderService = buildSenderService;
    }

    @PostMapping(value = "/start", consumes = APPLICATION_JSON_VALUE)
    public Map<String, Integer> start(
            @RequestBody BuildDtoRequest request,
            HttpServletResponse response
    ) {
        Optional<Land> land = landRepository.findById(request.getLandId());
        if (!land.isPresent()) {
            throw new ResponseException("FAIL. Land is empty");
        }
        if (request.getVersion() == null) {
            throw new ResponseException("FAIL. Version is empty");
        }
        Land entityLand = land.get();
        String pathDir = this.landPath + "/" + entityLand.getName();

        BuildVersion buildVersion = BuildVersion.builder()
                .versionName(request.getVersion())
                .landId(request.getLandId())
                .build();
        this.buildVersionRepository.saveAndFlush(buildVersion);
        BuildMessage message = new BuildMessage(pathDir, request.getVersion(), entityLand.getName(), buildVersion.getId());
        this.buildSenderService.send(message);
        Map<String, Integer> responseMap = new HashMap<String, Integer>();
        responseMap.put("buildId", buildVersion.getId());

        return responseMap;
    }


    @Data
    public static class BuildDtoRequest {
        private String version;
        private Integer landId;
    }
}
