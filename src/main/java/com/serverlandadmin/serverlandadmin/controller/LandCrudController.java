package com.serverlandadmin.serverlandadmin.controller;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.serverlandadmin.serverlandadmin.entity.Land;
import com.serverlandadmin.serverlandadmin.repository.LandRepository;
import com.serverlandadmin.serverlandadmin.repository.UserRepository;
import lombok.Data;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import java.time.ZonedDateTime;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/land")
public class LandCrudController {

    private final LandRepository landRepository;

    public LandCrudController(LandRepository landRepository) {
        this.landRepository = landRepository;
    }

    @RequestMapping(method = {RequestMethod.POST, RequestMethod.GET}, path ="/test")
    public String test() {
        return "fghj";
    }

    @PostMapping(value = "/create", consumes = APPLICATION_JSON_VALUE)
    public String create(
            @RequestBody LandDtoRequest request,
            HttpServletResponse response
    ) {
        boolean isExist = landRepository.findFirstByName(request.getName())
                .isPresent();
        if (isExist) {
           response.setStatus(HttpServletResponse.SC_BAD_REQUEST);

           return "FAIL";
        }
        Land land = Land.builder()
                .active(request.getActive())
                .name(request.getName())
                .description(request.getDescription())
                .build();
        landRepository.saveAndFlush(land);

        return "OK";
    }

    @RequestMapping(method = {RequestMethod.POST, RequestMethod.GET}, path = "/update/{id}")
    public Land update (
            @PathVariable Integer id,
            @RequestBody LandDtoRequest request
    ) {
        Land land = landRepository.findById(id).orElse(null);
        if (land != null) {
            land.setActive(request.getActive());

            landRepository.saveAndFlush(land);
        }
        return land;
    }
    @GetMapping(path = "/delete/{id}")
    public String delete (@PathVariable Integer id) {
        landRepository.deleteById(id);

        return "OK";
    }

    @Data
    public static class LandDtoRequest {
        private String name;
        private String description;
        private Boolean active;
    }
}
