package com.serverlandadmin.serverlandadmin.controller;

import com.serverlandadmin.serverlandadmin.entity.Environment;
import com.serverlandadmin.serverlandadmin.repository.EnvironmentRepository;
import lombok.Data;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/environment")
public class EnvironmentCrudController {
    private final EnvironmentRepository environmentRepository;

    public EnvironmentCrudController(EnvironmentRepository environmentRepository) {
        this.environmentRepository = environmentRepository;
    }
    @PostMapping(value = "/create", consumes = APPLICATION_JSON_VALUE)
    public String create (
            @RequestBody EnvironmentDtoRequest request
    ) {
        Environment environment = Environment.builder()
                .environmentName(request.getEnvironmentName())
                .host(request.getHost())
                .buildId(request.getBuildId())
                .active(request.getActive())
                .deployPath(request.getDeployPath())
                .sshKey(request.getSshKey())
                .passphrase(request.getPassphrase())
                .build();
        environmentRepository.saveAndFlush(environment);
        return "OK";
    }
    @Data
    public static class EnvironmentDtoRequest {
        private String host;
        private String environmentName;
        private Integer buildId = null;
        private Boolean active = false;
        private String deployPath;
        private String sshKey;
        private String passphrase;
    }
}
