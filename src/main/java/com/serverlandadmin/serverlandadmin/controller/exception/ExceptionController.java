package com.serverlandadmin.serverlandadmin.controller.exception;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletResponse;

@Service
public class ExceptionController {

    @ExceptionHandler({ Throwable.class })
    public String handler (Throwable throwable, HttpServletResponse response) {
        response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        return throwable.getMessage();
    }
}
