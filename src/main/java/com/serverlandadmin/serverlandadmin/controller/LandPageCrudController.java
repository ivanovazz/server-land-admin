package com.serverlandadmin.serverlandadmin.controller;

import com.serverlandadmin.serverlandadmin.entity.Land;
import com.serverlandadmin.serverlandadmin.repository.LandRepository;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Optional;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/land-page")
public class LandPageCrudController {
    @Value( "${land.path}" )
    private String landPath;

    private final LandRepository landRepository;

    public LandPageCrudController(LandRepository landRepository) {this.landRepository = landRepository;}

    @PostMapping(value = "/create", consumes = APPLICATION_JSON_VALUE)
    public String create(
            @RequestBody LandPageCrudController.LandPageDtoRequest request,
            HttpServletResponse response
    ) {
        Optional<Land> land = landRepository.findById(request.getLandId());
        if (!land.isPresent()) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);

            return "FAIL. Land is empty";
        }

        Land entityLand = land.get();

        File theDir = new File(this.landPath + "/" + entityLand.getName());
        if (!theDir.exists()){
            boolean isPresent = theDir.mkdirs();
            if (!isPresent) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);

                return "FAIL. Dirs cann't create";
            }
        }
        String filePath = this.landPath + "/" + entityLand.getName() + "/" + request.getName() + "." + request.getType();
        File landPageFile = new File(filePath);
        try {
            boolean isCreated = landPageFile.createNewFile();
            if (!isCreated) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);

                return "FAIL. File cann't create";
            }
            FileWriter fw = new FileWriter(landPageFile.getPath(), true);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter out = new PrintWriter(bw);
            out.println(request.getContent());
            out.close();
        } catch (Exception exception) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return exception.getMessage();
        }

        return "OK";
    }
    @Data
    public static class LandPageDtoRequest {
        private String name;
        private String content;
        private Integer landId;
        private Boolean type;
    }
}
