package com.serverlandadmin.serverlandadmin.repository;

import com.serverlandadmin.serverlandadmin.entity.Environment;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface EnvironmentRepository extends JpaRepository<Environment, Integer> {
}
