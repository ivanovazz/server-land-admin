package com.serverlandadmin.serverlandadmin.repository;

import com.serverlandadmin.serverlandadmin.entity.BuildVersion;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface BuildVersionRepository extends JpaRepository<BuildVersion, Integer> {
}
