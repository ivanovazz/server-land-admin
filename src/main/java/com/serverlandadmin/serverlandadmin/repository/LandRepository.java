package com.serverlandadmin.serverlandadmin.repository;

import com.serverlandadmin.serverlandadmin.entity.Land;
import lombok.NonNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface LandRepository extends JpaRepository<Land, Integer>{

    @NonNull
    Optional<Land> findFirstByName(@NonNull String name);

}
