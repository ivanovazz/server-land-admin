package com.serverlandadmin.serverlandadmin.service;

import com.serverlandadmin.serverlandadmin.dto.BuildMessage;

public interface BuildSenderServiceInterface {
    void send(BuildMessage message);
}
