package com.serverlandadmin.serverlandadmin.service;

import com.serverlandadmin.serverlandadmin.dto.BuildMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import static com.serverlandadmin.serverlandadmin.configuration.ActiveMQConfig.ORDER_QUEUE;
@Service
public class BuildSenderService implements BuildSenderServiceInterface{
    private static Logger log = LoggerFactory.getLogger(BuildSenderService.class);


    private final JmsTemplate jmsTemplate;

    public BuildSenderService (JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }

    @Override
    public void send(BuildMessage message) {
        log.info("sending to queue < artifactPath: " + message.getArtifactPath() + " dirPath: " + message.getDirPath() + " version: " + message.getVersion() + ">");
        jmsTemplate.convertAndSend(ORDER_QUEUE, message);
    }
}
