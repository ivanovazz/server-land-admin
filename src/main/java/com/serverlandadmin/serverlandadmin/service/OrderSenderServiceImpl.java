package com.serverlandadmin.serverlandadmin.service;

import com.serverlandadmin.serverlandadmin.dto.Order;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

import java.lang.reflect.Proxy;

import static com.serverlandadmin.serverlandadmin.configuration.ActiveMQConfig.ORDER_QUEUE;

@Service
public class OrderSenderServiceImpl implements OrderSenderService {
    private static Logger log = LoggerFactory.getLogger(OrderSenderServiceImpl.class);


    private final JmsTemplate jmsTemplate;


    public OrderSenderServiceImpl(JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
        System.out.println("RUN CONSTRUCTOR");
    }

    @PostConstruct
    public void initialize() {
        log.info("Run init");
    }

    public void send(Order myMessage) {
        log.info("sending with convertAndSend() to queue <" + myMessage + ">");
        jmsTemplate.convertAndSend(ORDER_QUEUE, myMessage);
    }

    public MakeHolder make() {
        String str = "sdf";
        Integer i = 123;

        this.showClass();

        return new MakeHolder(i, str);
    }

    public void showClass() {

        log.info(getClass().getName());
    }

}
