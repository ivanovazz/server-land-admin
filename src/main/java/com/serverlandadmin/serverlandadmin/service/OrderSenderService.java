package com.serverlandadmin.serverlandadmin.service;

import com.serverlandadmin.serverlandadmin.dto.Order;
import lombok.AllArgsConstructor;
import lombok.Getter;

public interface OrderSenderService {
    @Getter
    @AllArgsConstructor
    class MakeHolder {
        private final Integer i;
        private final String str;
    }

    void send(Order myMessage);

    MakeHolder make();

    void showClass();
}
