package com.serverlandadmin.serverlandadmin.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity(name = "users")
public class User {

    @Id
    private Integer id;

    @Column
    private String login;

    @Column
    private String password;

}
