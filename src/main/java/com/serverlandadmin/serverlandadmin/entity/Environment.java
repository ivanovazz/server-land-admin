package com.serverlandadmin.serverlandadmin.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.ZonedDateTime;

import static javax.persistence.GenerationType.IDENTITY;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Environment {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Integer id;

    @Column
    private String host;

    @Column(name="environment_name")
    private String environmentName;

    @Column(name="build_id")
    private Integer buildId;

    @Column(name="deploy_path")
    private String deployPath;

    @Column(name="ssh_key")
    private String sshKey;

    @Column
    private String passphrase;

    @Column(name="created_at", insertable = false, updatable = false)
    private ZonedDateTime createdAt;

    @Column(name="updated_at", insertable = false)
    private ZonedDateTime updatedAt;

    @Column
    private Boolean active;

    @PreUpdate
    public void beforeSave() {
        this.setUpdatedAt(ZonedDateTime.now());
    }
}
