package com.serverlandadmin.serverlandadmin.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.ZonedDateTime;

import static javax.persistence.GenerationType.IDENTITY;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "build_version")
public class BuildVersion {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Integer id;

    @Column
    private String host;

    @Column(name="version_name")
    private String versionName;

    @Column(name="content")
    private String content;

    @Column(name = "land_id")
    private Integer landId;


    @Column(name="created_at", insertable = false, updatable = false)
    private ZonedDateTime createdAt;

    @Column(name="updated_at", insertable = false)
    private ZonedDateTime updatedAt;

    @Column
    private Boolean active;

    @PreUpdate
    public void beforeSave() {
        this.setUpdatedAt(ZonedDateTime.now());
    }
}
