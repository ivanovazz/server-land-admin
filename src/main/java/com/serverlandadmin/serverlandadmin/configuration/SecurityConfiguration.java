package com.serverlandadmin.serverlandadmin.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .authorizeRequests()
                .antMatchers("/**").permitAll()
//                .mvcMatchers(HttpMethod.POST, "/land/**").permitAll()
//                .mvcMatchers(HttpMethod.GET, "/land/**").permitAll()
                .and()
                .csrf().disable();;
    }
}
